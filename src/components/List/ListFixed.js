import ListGroup from 'react-bootstrap/ListGroup';
import { useState } from 'react';
import ListItem from '../ListItem/ListItem';

function List() {
  const [list,setList] = useState([1,2,3,4,5,6,7,8,9,10])

  const handleClick = (index,item) => {
    const array = [  ...list  ];
    let deleteIndex=index;

    if(index===0){deleteIndex=null}else if(item%2===0) { deleteIndex=index + 1}
    console.log(index,deleteIndex,array.slice(0,deleteIndex),array.slice(deleteIndex+1,array.length))

    if (deleteIndex) { setList(array.slice(0, deleteIndex).concat(array.slice(deleteIndex + 1, array.length)))}
  }

  console.log(handleClick, ' onDelete={handleClick}');

  return (
    <ListGroup className="list-container">
    {list.map((item, index) => <ListItem key={item} item={item} index={"" + index} onClick={handleClick}/>)}
    </ListGroup>
  );
}

export default List;
