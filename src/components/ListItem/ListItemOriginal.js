import Item from 'react-bootstrap/ListGroupItem';
import Button from 'react-bootstrap/Button';

const ListItem = ({item, index, onDelete}) => <Item className="list-item" key={item}>
<div>List Item {item}</div>
<Button variant="outline-danger" onClick={() => onDelete(index, item)}>
{item===1 ? 'Does nothing':`Delete ${item%2===0 ? 'next':'this'}`}
</Button>
</Item>

export default ListItem;