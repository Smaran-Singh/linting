// import Item from 'react-bootstrap/ListGroupItem';
// import Button from 'react-bootstrap/Button';
// import PropTypes from 'prop-types';

// /**
//  * Component to render each item of the list
//  * 
//  * @component
//  * @example
//  * const item = 1
//  * const index = 0
//  * const onDelete = () => {}
//  * return (
//  *   <ListItem item={item} index={index} onDelete={onDelete}/>
//  * )
//  */
// const ListItem = ({ item, index, onDelete = () => {} }) => (
//   <Item className="list-item" key={item}>
//     <div>List Item {item}</div>
//     <Button variant="outline-danger" onClick={() => onDelete(index, item)}>
//       {item === 1 ? 'Does nothing' : `Delete ${item % 2 === 0 ? 'next' : 'this'}`}
//     </Button>
//   </Item>
// );

// ListItem.propTypes = {
//   /**
//    * The element from the list
//    */
//   item: PropTypes.number,
//   /**
//    * index of the element in the list
//    */
//   index: PropTypes.number,
//   /**
//    * function to handle the delete button click
//    */
//   onDelete: PropTypes.func,
// };

// ListItem.defaultProps = {
//   item: '',
//   index: null,
// };

// export default ListItem;
