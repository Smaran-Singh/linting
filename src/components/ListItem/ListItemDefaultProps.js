import Item from 'react-bootstrap/ListGroupItem';
import Button from 'react-bootstrap/Button';
import PropTypes from 'prop-types';

const ListItem = ({ item, index, onDelete = () => {} }) => (
  <Item className="list-item" key={item}>
    <div>List Item {item}</div>
    <Button variant="outline-danger" onClick={() => onDelete(index, item)}>
      {item === 1 ? 'Does nothing' : `Delete ${item % 2 === 0 ? 'next' : 'this'}`}
    </Button>
  </Item>
);

ListItem.propTypes = {
  item: PropTypes.number,
  index: PropTypes.number,
  onDelete: PropTypes.func,
};

ListItem.defaultProps = {
  item: '',
  index: null,
}

export default ListItem;
